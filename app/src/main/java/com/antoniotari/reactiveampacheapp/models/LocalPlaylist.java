/*
 * Power Ampache, Ampache player for Android
 * Copyright (C) 2016  Antonio Tari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.antoniotari.reactiveampacheapp.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import com.antoniotari.reactiveampache.models.Playlist;
import com.antoniotari.reactiveampache.models.Song;

/**
 * Created by antonio tari on 2016-07-21.
 */
public class LocalPlaylist extends Playlist {

    private List<Song> songList;

    public LocalPlaylist(List<Song> songList, String name) {
        if(songList != null) {
            this.songList = songList;
        } else {
            this.songList = new ArrayList<>();
        }
        setName(name);
        setItems(this.songList.size());
    }

    public List<Song> getSongList() {
        return songList;
    }

    protected LocalPlaylist(Parcel in) {
        super(in);

        if (in.readByte() == 0x01) {
            songList = new ArrayList<Song>();
            in.readList(songList, Song.class.getClassLoader());
        } else {
            songList = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest,flags);

        if (songList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(songList);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<LocalPlaylist> CREATOR = new Parcelable.Creator<LocalPlaylist>() {
        @Override
        public LocalPlaylist createFromParcel(Parcel in) {
            return new LocalPlaylist(in);
        }

        @Override
        public LocalPlaylist[] newArray(int size) {
            return new LocalPlaylist[size];
        }
    };
}
