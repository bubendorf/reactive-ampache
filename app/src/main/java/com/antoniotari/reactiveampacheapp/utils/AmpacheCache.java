package com.antoniotari.reactiveampacheapp.utils;

import android.content.Context;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import com.antoniotari.reactiveampache.models.Album;
import com.antoniotari.reactiveampache.models.Artist;
import com.antoniotari.reactiveampache.models.Playlist;
import com.antoniotari.reactiveampache.models.Song;
import com.antoniotari.reactiveampache.utils.FileUtil;
import com.antoniotari.reactiveampache.utils.Log;
import com.antoniotari.reactiveampache.utils.SerializeUtils;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import rx.Observable;
import rx.Observable.OnSubscribe;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by antoniotari on 2017-01-09.
 */

public enum AmpacheCache {
    INSTANCE;

    private OkHttpClient mOkHttpClient;

    public static final Executor SERIAL_EXECUTOR = Executors.newSingleThreadExecutor();

    private final Map<String,Artist> mArtistMap = new ArrayMap<>();
    private final Map<String,Album> mAlbumMap = new ArrayMap<>();
    private final Map<String,Song> mSongMap = new ArrayMap<>();
    private final Map<String,Playlist> mPlaylistMap = new ArrayMap<>();

    public void addArtistToCache(Artist artist) {
        mArtistMap.put(artist.getId(),artist);
    }

    public void addAlbumToCache(Album album) {
        mAlbumMap.put(album.getId(),album);
    }

    public void addPlaylistToCache(Playlist playlist) {
        mPlaylistMap.put(playlist.getName(), playlist);
    }

    public void addSongToCache(Song song) {
        mSongMap.put(song.getId(),song);
    }

    public Artist getArtist(String id) {
        return mArtistMap.get(id);
    }

    public Album getAlbum(String id) {
        return mAlbumMap.get(id);
    }

    public Song getSong(String id) {
        return mSongMap.get(id);
    }

    public Playlist getPlaylist(final String playlistName) {
        return mPlaylistMap.get(playlistName);
    }

    private OkHttpClient getOkHttpClient() {
        if (mOkHttpClient == null) {
            mOkHttpClient = new OkHttpClient();
        }
        return mOkHttpClient;
    }

    private String getFileName(final Song song) {
        return song.getArtist().getId() + "_" + song.getId();
    }

    private File getSaveDir(Context context) {
        return context.getFilesDir();
    }

    public Observable<Song> downloadSong(Context context, final Song song) {
        String filename = getFileName(song);
        final File file = new File(getSaveDir(context), filename);
        return Observable.create((OnSubscribe<Song>) subscriber -> {
            try {
                Log.blu("before", song.getTitle());
                copyInputStreamToFile(downloadBinaryFile(song.getUrl()), file);
                Log.blu("after", song.getTitle());
                // writing song data to file
                FileUtil.getInstance().writeStringFile(context, filename + ".song",new SerializeUtils().toJsonString(song));
                subscriber.onNext(song);
                subscriber.onCompleted();
            } catch (Exception e) {
                subscriber.onError(e);
            }
        }).subscribeOn(Schedulers.from(SERIAL_EXECUTOR)).observeOn(AndroidSchedulers.mainThread());
    }

    private InputStream downloadBinaryFile(final String url) throws IOException {
        Request request = new Request.Builder().url(url).build();
        Response response = getOkHttpClient().newCall(request).execute();
        if (!response.isSuccessful()) {
            throw new IOException("Unexpected code " + response);
        }
        return response.body().byteStream();
    }

    private void copyInputStreamToFile(InputStream in, File file) throws IOException {
        OutputStream out = new FileOutputStream(file);
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        out.close();
        in.close();
    }

    public boolean isSongInCache(Context context, Song song) {
        File file = new File(getSaveDir(context), getFileName(song));
        return (file.exists());
    }

    public void deleteSongFromCache(Context context, Song song) {
        File fileData = new File(getSaveDir(context), getFileName(song)+".song");
        if(fileData.delete()) {
            File file = new File(getSaveDir(context), getFileName(song));
            file.delete();
        }
    }

    public String getCachedSongPath(Context context, Song song) {
        File file = new File(getSaveDir(context), getFileName(song));
        if (file.exists()) {
            return file.getPath();
        }
        return null;
    }

    public List<Song> getCachedList(Context context) {
        SerializeUtils serializeUtils = new SerializeUtils();
        List<Song> songs = new ArrayList<>();
        Queue<File> files = new LinkedList<>();
        files.addAll(Arrays.asList(getSaveDir(context).listFiles()));
        Log.log("antonio", files.size());

        while (!files.isEmpty()) {
            File file = files.remove();
            Log.log("antonio", file.getName());
            if (file.isDirectory()) {
                //files.addAll(Arrays.asList(file.listFiles()));
            } else if (file.getName().endsWith(".song")) {
                String jsonSong = FileUtil.getInstance().readStringFile(context, file.getName());
                if (!TextUtils.isEmpty(jsonSong)) {
                    Song song = serializeUtils.fromJson(jsonSong, Song.class);
                    songs.add(song);
                    Log.log("antonio", song.getTitle());
                }
            }
        }

        Collections.sort(songs, (Song o1, Song o2) -> {
            String title1 = o1.getArtist().getName() + o1.getTitle();
            String title2 = o2.getArtist().getName() + o2.getTitle();
            return title1.compareTo(title2);
        });

        return songs;
    }
}
