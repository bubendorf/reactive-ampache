/*
 * Power Ampache, Ampache player for Android
 * Copyright (C) 2016  Antonio Tari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.antoniotari.reactiveampacheapp.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.antoniotari.reactiveampache.models.AmpacheModel;
import com.antoniotari.reactiveampacheapp.R;
import com.antoniotari.reactiveampacheapp.utils.FastScrollWrapper;
import com.antoniotari.reactiveampacheapp.utils.Utils;

import xyz.danoz.recyclerviewfastscroller.sectionindicator.title.SectionTitleIndicator;
import xyz.danoz.recyclerviewfastscroller.vertical.VerticalRecyclerViewFastScroller;

/**
 * Created by antonio tari on 2016-05-29.
 */
public abstract class BaseFragment<T extends AmpacheModel> extends Fragment implements Filterable {

    protected RecyclerView recyclerView;
    protected ProgressBar progressWait;
    private SwipeRefreshLayout swipeLayout;

    protected FastScrollWrapper mFastScrollWrapper;

    private String filterText;

    protected abstract void onRefresh();
    protected abstract void initialize();

    protected void initViews(View rootView) {
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerview);
        progressWait = (ProgressBar) rootView.findViewById(R.id.progressBarWait);
        swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);

        swipeLayout.setOnRefreshListener(this::onRefresh);
        swipeLayout.setColorSchemeResources(R.color.colorAccent, R.color.colorPrimary, R.color.colorPrimaryDark);

        // initialize fast scrolling
        VerticalRecyclerViewFastScroller fastScroller = (VerticalRecyclerViewFastScroller) rootView.findViewById(R.id.fast_scroller);
        SectionTitleIndicator sectionTitleIndicator = (SectionTitleIndicator) rootView.findViewById(R.id.fast_scroller_section_title_indicator);
        mFastScrollWrapper = new FastScrollWrapper(recyclerView, fastScroller, sectionTitleIndicator);

        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), getResources().getInteger(R.integer.column_num)));
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.list_base, container, false);
        initViews(rootView);
        initialize();
        return rootView;
    }

    protected void startWaiting() {
        //swipeLayout.setRefreshing(true);
        progressWait.setVisibility(View.VISIBLE);
    }

    protected void stopWaiting() {
        swipeLayout.setRefreshing(false);
        progressWait.setVisibility(View.GONE);
    }

    public void onError(Throwable throwable) {
        Utils.onError(getActivity(), throwable);
        stopWaiting();
    }

    @Override
    public void setFilterText(String filterText) {
        this.filterText = filterText;
    }

    @Override
    public String getFilterText() {
        return filterText;
    }
}
