package com.antoniotari.reactiveampacheapp.ui.activities;

import com.antoniotari.reactiveampache.models.Playlist;
import com.antoniotari.reactiveampacheapp.models.LocalPlaylist;

/**
 * Created by antoniotari on 2017-04-20.
 */

public class OfflinePlaylistActivity extends PlaylistActivity {

    @Override
    protected void loadSongs(Playlist playlist) {
        initSongList(((LocalPlaylist)playlist).getSongList());
    }

    @Override
    protected boolean isLocalPlaylist() {
        return false;
    }

    @Override
    protected void onRefresh() {
        swipeLayout.setRefreshing(false);
    }
}
