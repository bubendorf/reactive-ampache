package com.antoniotari.reactiveampacheapp.ui.fragments;

/**
 * Created by mbu on 18.05.17.
 */

public interface Filterable {
    void setFilterText(String filterText);
    String getFilterText();
}
