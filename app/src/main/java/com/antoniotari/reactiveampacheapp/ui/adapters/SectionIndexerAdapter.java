package com.antoniotari.reactiveampacheapp.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.widget.SectionIndexer;

import com.antoniotari.reactiveampache.models.AmpacheModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by antoniotari on 2017-03-13.
 */

abstract class SectionIndexerAdapter<T extends AmpacheModel, V extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<V> implements SectionIndexer {

    private List<T> mItems;
    private List<T> mFilteredItems = new ArrayList<>();
    private String mFilterText;

    public SectionIndexerAdapter(List<T> items) {
        mItems = items;
        applyFilter();
    }

    protected void setItems(List<T> items) {
        mItems = items;
        applyFilter();
    }

    @Override
    public Object[] getSections() {
        return getItems().toArray();
    }

    @Override
    public int getPositionForSection(final int sectionIndex) {
        return 0;
    }

    @Override
    public int getSectionForPosition(int position) {
        if (position >= getItems().size()) {
            position = getItems().size() - 1;
        }
        AmpacheModel ampacheModel = getItems().get(position);
        return getItems().indexOf(ampacheModel);
    }

    public void setFilterText(String filterText) {
        if (!equals(mFilteredItems, filterText)) {
            mFilterText = filterText;
            applyFilter();
        }
    }

    public static boolean equals(Object a, Object b) {
        return (a == b) || (a != null && a.equals(b));
    }

    private void applyFilter() {
        mFilteredItems.clear();
        if (mItems != null || mItems.size() > 0) {
            if (mFilterText == null || mFilterText.length() == 0) {
                mFilteredItems.addAll(mItems);
            } else {
                String text = mFilterText.toLowerCase();
                for (T item : mItems) {
                    if ( item.getName().toLowerCase().contains(text)) {
                        mFilteredItems.add(item);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mFilteredItems.size();
    }

    protected List<T> getItems() {
        return mFilteredItems;
    }
}
